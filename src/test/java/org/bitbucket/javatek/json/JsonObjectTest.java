package org.bitbucket.javatek.json;

import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class JsonObjectTest {
  @Test
  public void test() throws IOException {
    String json = readJsonFromResource("/JsonObjectTest.json");
    JsonObject object = new JsonObject(json);

    assertEquals(json, object.toString(JsonStyle.PRETTY));
    assertEquals(8, object.size());
    assertTrue(object.has("name"));
    assertEquals("Иван", object.getString("name"));
    assertTrue(object.getBoolean("married"));
    assertEquals(37, object.getInt("age"));
    assertEquals(37L, object.getLong("age"));
    assertEquals(new BigDecimal("37"), object.getNumber("age"));
    assertEquals(new URL("http://www.ivan.me"), object.getURL("site"));

    assertNotNull(object.getJsonObject("mother"));
    assertNotNull(object.getJsonArray("children"));

    JsonObject timestamp = object.getJsonObject("timestamp");
    assertEquals(33, timestamp.getSeconds("seconds").getSeconds());
    assertEquals(499, timestamp.getMillis("millis").toMillis());
  }

  private String readJsonFromResource(String resourceName) {
    try {
      URL resourceUrl = getClass().getResource(resourceName);
      String resourceAsString = new String(
        Files.readAllBytes(Paths.get(resourceUrl.toURI())),
        UTF_8);
      return resourceAsString.replace("\r\n", "\n");
    }
    catch (URISyntaxException | IOException e) {
      throw new AssertionError(e);
    }
  }
}