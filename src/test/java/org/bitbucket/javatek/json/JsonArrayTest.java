package org.bitbucket.javatek.json;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JsonArrayTest {
  @Test
  public void test() throws IOException {
    String json = readJsonFromResource("/JsonArrayTest.json");
    JsonArray array = new JsonArray(json);

    assertEquals(json, array.toString(JsonStyle.PRETTY));
    assertEquals(6, array.size());
    assertTrue(array.get(0).isJsonObject());
    assertTrue(array.get(1).isJsonArray());
    assertTrue(array.get(2).isString());
    assertTrue(array.get(3).isNumber());
    assertTrue(array.get(4).isBoolean());
    assertTrue(array.get(5).isNull());
  }

  private String readJsonFromResource(String resourceName) {
    try {
      URL resourceUrl = getClass().getResource(resourceName);
      String resourceAsString = new String(
        Files.readAllBytes(Paths.get(resourceUrl.toURI())),
        UTF_8);
      return resourceAsString.replace("\r\n", "\n");
    }
    catch (URISyntaxException | IOException e) {
      throw new AssertionError(e);
    }
  }
}