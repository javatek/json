package org.bitbucket.javatek.json;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.Math.min;

/**
 *
 */
public final class JsonArray implements Iterable<JsonElement> {
  private final List<JsonElement> elements;

  public JsonArray(List<JsonElement> elements) {
    this.elements = new ArrayList<>(elements);
  }

  public JsonArray(String string) throws IOException {
    try (Reader fileReader = new StringReader(string)) {
      try (JsonReader jsonReader = new JsonReader(fileReader)) {
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(jsonReader);
        if (!json.isJsonArray())
          throw new IOException(
            "Not a JSON Array: " + string.substring(0, min(50, string.length())) + "...");
        this.elements = json.asJsonArray().elements;
      }
    }
  }

  public JsonArray(File file, Charset charset) throws IOException {
    try (InputStream fileStream = new FileInputStream(file)) {
      try (Reader fileReader = new InputStreamReader(fileStream, charset)) {
        try (JsonReader jsonReader = new JsonReader(fileReader)) {
          JsonParser parser = new JsonParser();
          JsonElement json = parser.parse(jsonReader);
          if (!json.isJsonArray())
            throw new IOException("Not a JSON Array: " + file);
          this.elements = json.asJsonArray().elements;
        }
      }
    }
  }

  public int size() {
    return elements.size();
  }

  @Nonnull
  public Iterator<JsonElement> iterator() {
    return elements.iterator();
  }

  @Nonnull
  public Stream<JsonElement> stream() {
    return elements.stream();
  }

  @Nonnull
  public JsonElement get(int index) throws IndexOutOfBoundsException {
    return elements.get(index);
  }

  @Override
  public String toString() {
    return toString(JsonStyle.DEFAULT);
  }

  public String toString(JsonStyle style) {
    try {
      StringWriter stringWriter = new StringWriter();
      new JsonWriter(stringWriter, style)
        .write(new JsonElement(this));
      return stringWriter.toString();
    }
    catch (IOException e) {
      throw new AssertionError(e);
    }
  }

  @Override
  public int hashCode() {
    return elements.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return (o == this) || (
      o != null
        && o.getClass() == JsonArray.class
        && ((JsonArray) o).elements.equals(this.elements));
  }
}
