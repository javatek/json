package org.bitbucket.javatek.json;

/**
 *
 */
public interface JsonStyle {
  String indent();

  /**
   * Used only if indent is non empty
   */
  String newLine();

  /**
   * The name/value separator; either ":" or ": ".
   */
  String separator();

  /**
   * Configure to emit JSON that's safe for direct inclusion in HTML
   * and XML documents. This escapes the HTML characters {@code <}, {@code >},
   * {@code &} and {@code =} before writing them to the stream. Without this
   * setting, your XML/HTML encoder should replace these characters with the
   * corresponding escape sequences.
   */
  default boolean htmlSafe() { return false; }

  /**
   * Sets whether object members are serialized when their value is null.
   * This has no impact on array elements. The default is true.
   */
  default boolean serializeNulls() { return true; }

  //////////////////////////////////////////////////////////////////////////////

  interface Compact extends JsonStyle {
    @Override
    default String indent() { return null; }

    @Override
    default String newLine() { return null; }

    @Override
    default String separator() { return ":"; }
  }

  interface Pretty extends JsonStyle {
    @Override
    default String indent() { return "  "; }

    @Override
    default String newLine() { return "\n"; }

    @Override
    default String separator() { return ": "; }
  }

  JsonStyle COMPACT = new Compact() {};
  JsonStyle PRETTY = new Pretty() {};
  JsonStyle DEFAULT = COMPACT;
}
