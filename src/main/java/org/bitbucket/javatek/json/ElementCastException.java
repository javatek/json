package org.bitbucket.javatek.json;

/**
 *
 */
public final class ElementCastException extends RuntimeException {
  private static final long serialVersionUID = 8046447385229663472L;

  ElementCastException(String message) {
    super(message);
  }
}
