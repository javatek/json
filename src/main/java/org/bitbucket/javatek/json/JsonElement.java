package org.bitbucket.javatek.json;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;


/**
 *
 */
public final class JsonElement {
  static final JsonElement NULL = new JsonElement();

  private final JsonObject jsonObject;
  private final JsonArray jsonArray;
  private final String string;
  private final BigDecimal number;
  private final Boolean bool;

  public JsonElement(JsonObject jsonObject) {
    this.jsonObject = requireNonNull(jsonObject);
    this.jsonArray = null;
    this.string = null;
    this.number = null;
    this.bool = null;
  }

  public JsonElement(JsonArray jsonArray) {
    this.jsonObject = null;
    this.jsonArray = requireNonNull(jsonArray);
    this.string = null;
    this.number = null;
    this.bool = null;
  }

  public JsonElement(String string) {
    this.jsonObject = null;
    this.jsonArray = null;
    this.string = requireNonNull(string);
    this.number = null;
    this.bool = null;
  }

  public JsonElement(BigDecimal number) {
    this.jsonObject = null;
    this.jsonArray = null;
    this.string = null;
    this.number = requireNonNull(number);
    this.bool = null;
  }

  public JsonElement(boolean bool) {
    this.jsonObject = null;
    this.jsonArray = null;
    this.string = null;
    this.number = null;
    this.bool = bool;
  }

  private JsonElement() {
    this.jsonObject = null;
    this.jsonArray = null;
    this.string = null;
    this.number = null;
    this.bool = null;
  }

  public boolean isJsonArray()  {return jsonArray != null;}

  public boolean isJsonObject() {return jsonObject != null;}

  public boolean isString()     {return string != null;}

  public boolean isNumber()     {return number != null;}

  public boolean isBoolean()    {return bool != null;}

  public boolean isNull() {
    return this == NULL || (
      jsonObject == null
        && jsonArray == null
        && string == null
        && number == null
        && bool == null);
  }

  @Nonnull
  public JsonObject asJsonObject() throws ElementCastException {
    if (jsonObject != null)
      return jsonObject;
    throw new ElementCastException("JSON Object expected");
  }

  @Nonnull
  public JsonArray asJsonArray() throws ElementCastException {
    if (jsonArray != null)
      return jsonArray;
    throw new ElementCastException("JSON Array expected");
  }

  @Nonnull
  public String asString() throws ElementCastException {
    if (string != null)
      return string;
    if (number != null)
      return number.toPlainString();
    if (bool != null)
      return bool.toString();
    throw new ElementCastException("JSON String expected");
  }

  @Nonnull
  public BigDecimal asNumber() throws ElementCastException {
    if (number != null)
      return number;
    throw new ElementCastException("JSON Number expected");
  }

  public boolean asBoolean() throws ElementCastException {
    if (bool != null)
      return bool;
    throw new ElementCastException("JSON Boolean expected");
  }

  @Override
  public String toString() {
    if (jsonObject != null)
      return jsonObject.toString();
    if (jsonArray != null)
      return jsonArray.toString();
    if (string != null)
      return string;
    if (number != null)
      return number.toPlainString();
    if (bool != null)
      return bool ? "true" : "false";
    return "null";
  }

  @Override
  public int hashCode() {
    if (jsonObject != null)
      return jsonObject.hashCode();
    if (jsonArray != null)
      return jsonArray.hashCode();
    if (string != null)
      return string.hashCode();
    if (number != null)
      return number.hashCode();
    if (bool != null)
      return Boolean.hashCode(bool);
    return "null".hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if (o != null && o.getClass() == JsonElement.class) {
      JsonElement other = (JsonElement) o;
      if (this.isJsonObject() && other.isJsonObject())
        return this.asJsonObject().equals(other.asJsonObject());
      if (this.isJsonArray() && other.isJsonArray())
        return this.asJsonArray().equals(other.asJsonArray());
      if (this.isString() && other.isString())
        return this.asString().equals(other.asString());
      if (this.isNumber() && other.isNumber())
        return 0 == this.asNumber().compareTo(other.asNumber());
      if (this.isBoolean() && other.isBoolean())
        return this.asBoolean() == other.asBoolean();
      return this.isNull() == other.isNull();
    }

    return false;
  }
}
