package org.bitbucket.javatek.json;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
final class JsonParser {
  JsonElement parse(JsonReader reader) throws IOException {
    try {
      boolean isEmpty = true;
      try {
        reader.peek();
        isEmpty = false;
        return read(reader);
      }
      catch (EOFException e) {
        /*
         * For compatibility with JSON 1.5 and earlier, we return a JsonNull for
         * empty documents instead of throwing.
         */
        if (isEmpty) {
          return JsonElement.NULL;
        }
        // The stream ended prematurely so it is likely a syntax error.
        throw e;
      }
      catch (NumberFormatException e) {
        throw new IOException(e);
      }
    }
    catch (StackOverflowError | OutOfMemoryError e) {
      throw new IOException("Failed parsing JSON source: " + reader + " to Json", e);
    }
  }

  private JsonElement read(JsonReader in) throws IOException, NumberFormatException {
    JsonToken token = in.peek();
    switch (token) {
      case STRING:
        return new JsonElement(in.nextString());
      case NUMBER:
        String number = in.nextString();
        return new JsonElement(new BigDecimal(number));
      case BOOLEAN:
        return new JsonElement(in.nextBoolean());
      case NULL:
        in.nextNull();
        return JsonElement.NULL;
      case BEGIN_ARRAY:
        List<JsonElement> array = new ArrayList<>();
        in.beginArray();
        while (in.hasNext()) {
          array.add(read(in));
        }
        in.endArray();
        return new JsonElement(new JsonArray(array));
      case BEGIN_OBJECT:
        Map<String, JsonElement> map = new LinkedHashMap<>();
        in.beginObject();
        while (in.hasNext()) {
          map.put(in.nextName(), read(in));
        }
        in.endObject();
        return new JsonElement(new JsonObject(map));
      case END_DOCUMENT:
      case NAME:
      case END_OBJECT:
      case END_ARRAY:
      default:
        throw new UnsupportedOperationException("Unexpected token " + token);
    }
  }
}
