package org.bitbucket.javatek.json;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.DateTimeException;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import static java.lang.Math.min;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Collections.emptyMap;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class JsonObject {
  private final Map<String, JsonElement> members;

  JsonObject(Map<String, JsonElement> members) {
    this.members = requireNonNull(members);
  }

  public JsonObject() {
    this(emptyMap());
  }

  public JsonObject(String string) throws IOException {
    try (Reader fileReader = new StringReader(string)) {
      try (JsonReader jsonReader = new JsonReader(fileReader)) {
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(jsonReader);
        if (!json.isJsonObject())
          throw new IOException(
            "Not a JSON Object: " + string.substring(0, min(50, string.length())) + "...");
        this.members = json.asJsonObject().members;
      }
    }
  }

  public JsonObject(File file, Charset charset) throws IOException {
    try (InputStream fileStream = new FileInputStream(file)) {
      try (Reader fileReader = new InputStreamReader(fileStream, charset)) {
        try (JsonReader jsonReader = new JsonReader(fileReader)) {
          JsonParser parser = new JsonParser();
          JsonElement json = parser.parse(jsonReader);
          if (!json.isJsonObject())
            throw new IOException("Not a JSON Object: " + file);
          this.members = json.asJsonObject().members;
        }
      }
    }
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable String string) {
    return
      string != null
        ? with(key, new JsonElement(string))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable Boolean bool) {
    return
      bool != null
        ? with(key, new JsonElement(bool))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable BigDecimal number) {
    return
      number != null
        ? with(key, new JsonElement(number))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable Integer number) {
    return
      number != null
        ? with(key, new JsonElement(BigDecimal.valueOf(number)))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable JsonObject object) {
    return
      object != null
        ? with(key, new JsonElement(object))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable JsonArray array) {
    return
      array != null
        ? with(key, new JsonElement(array))
        : with(key, JsonElement.NULL);
  }

  public <T> JsonObject with(@Nonnull String key, @Nullable T element) {
    return
      element != null
        ? with(key, new JsonElement(element.toString()))
        : with(key, JsonElement.NULL);
  }

  @Nonnull
  public JsonObject with(@Nonnull String key, @Nullable JsonElement element) {
    requireNonEmpty(key);
    if (element == null) {
      element = JsonElement.NULL;
    }
    Map<String, JsonElement> newMembers = new LinkedHashMap<>(members);
    newMembers.put(key, element);
    return new JsonObject(newMembers);
  }

  //////////////////////////////////////////////////////////////////////////////

  public int size() {
    return members.size();
  }

  public boolean has(String key) {
    return members.containsKey(key);
  }

  @Nonnull
  public JsonElement get(String key) throws NoSuchElementException {
    JsonElement member = members.get(key);
    if (member == null)
      throw new NoSuchElementException(key + " not found");
    return member;
  }

  @Nonnull
  public String getString(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    return element.asString();
  }

  @Nonnull
  public URL getURL(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    try {
      return new URL(element.asString());
    }
    catch (MalformedURLException e) {
      throw new ElementCastException(key + "URL expected");
    }
  }

  @Nonnull
  public BigDecimal getNumber(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    return element.asNumber();
  }

  public int getInt(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    try {
      return element.asNumber().intValueExact();
    }
    catch (ArithmeticException e) {
      throw new ElementCastException("Int expected");
    }
  }

  public long getLong(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    try {
      return element.asNumber().longValueExact();
    }
    catch (ArithmeticException e) {
      throw new ElementCastException("Long expected");
    }
  }

  @Nonnull
  public Duration getSeconds(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    try {
      long seconds = element.asNumber().longValueExact();
      return Duration.of(seconds, SECONDS);
    }
    catch (ArithmeticException | DateTimeException e) {
      throw new ElementCastException("Seconds expected");
    }
  }

  @Nonnull
  public Duration getMillis(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    try {
      long millis = element.asNumber().longValueExact();
      return Duration.of(millis, MILLIS);
    }
    catch (ArithmeticException | DateTimeException e) {
      throw new ElementCastException("Millis expected");
    }
  }

  public boolean getBoolean(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    return element.asBoolean();
  }

  @Nonnull
  public JsonObject getJsonObject(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    return element.asJsonObject();
  }

  @Nonnull
  public JsonArray getJsonArray(String key) throws NoSuchElementException, ElementCastException {
    JsonElement element = get(key);
    return element.asJsonArray();
  }

  //////////////////////////////////////////////////////////////////////////////

  public Set<Map.Entry<String, JsonElement>> entrySet() {
    return members.entrySet();
  }

  @Override
  public String toString() {
    return toString(JsonStyle.DEFAULT);
  }

  public String toString(JsonStyle style) {
    try {
      StringWriter stringWriter = new StringWriter();
      new JsonWriter(stringWriter, style)
        .write(new JsonElement(this));
      return stringWriter.toString();
    }
    catch (IOException e) {
      throw new AssertionError(e);
    }
  }

  @Override
  public int hashCode() {
    return members.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return (o == this) || (
      o != null
        && o.getClass() == JsonObject.class
        && ((JsonObject) o).members.equals(this.members));
  }
}
