package org.bitbucket.javatek.json;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Map;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;


/**
 * Non lenient json writer
 * @author Jesse Wilson
 */
final class JsonWriter implements Closeable, Flushable {
  /*
   * From RFC 7159, "All Unicode characters may be placed within the
   * quotation marks except for the characters that must be escaped:
   * quotation mark, reverse solidus, and the control characters
   * (U+0000 through U+001F)."
   *
   * We also escape '\u2028' and '\u2029', which JavaScript interprets as
   * newline characters. This prevents eval() from failing with a syntax
   * error. http://code.google.com/p/google-gson/issues/detail?id=341
   */
  private static final String[] REPLACEMENT_CHARS;
  private static final String[] HTML_SAFE_REPLACEMENT_CHARS;

  static {
    REPLACEMENT_CHARS = new String[128];
    for (int i = 0; i <= 0x1f; i++) {
      REPLACEMENT_CHARS[i] = String.format("\\u%04x", i);
    }
    REPLACEMENT_CHARS['"'] = "\\\"";
    REPLACEMENT_CHARS['\\'] = "\\\\";
    REPLACEMENT_CHARS['\t'] = "\\t";
    REPLACEMENT_CHARS['\b'] = "\\b";
    REPLACEMENT_CHARS['\n'] = "\\n";
    REPLACEMENT_CHARS['\r'] = "\\r";
    REPLACEMENT_CHARS['\f'] = "\\f";
    HTML_SAFE_REPLACEMENT_CHARS = REPLACEMENT_CHARS.clone();
    HTML_SAFE_REPLACEMENT_CHARS['<'] = "\\u003c";
    HTML_SAFE_REPLACEMENT_CHARS['>'] = "\\u003e";
    HTML_SAFE_REPLACEMENT_CHARS['&'] = "\\u0026";
    HTML_SAFE_REPLACEMENT_CHARS['='] = "\\u003d";
    HTML_SAFE_REPLACEMENT_CHARS['\''] = "\\u0027";
  }

  /**
   * The output data, containing at most one top-level array or object.
   */
  private final Writer out;
  private final JsonStyle style;

  private int[] stack = new int[32];
  private int stackSize = 0;

  private String deferredName;

  /**
   * Creates a new instance that writes a JSON-encoded stream to {@code out}.
   * For best performance, ensure {@link Writer} is buffered; wrapping in
   * {@link java.io.BufferedWriter BufferedWriter} if necessary.
   */
  JsonWriter(Writer out, JsonStyle style) {
    this.out = requireNonNull(out);
    this.style = requireNonNull(style);
    push(JsonScope.EMPTY_DOCUMENT);
  }

  JsonWriter(Writer out) {
    this(out, JsonStyle.DEFAULT);
  }

  void write(JsonElement value) throws IOException {
    if (value == null || value.isNull()) {
      nullValue();
    }
    else if (value.isNumber()) {
      value(value.asNumber());
    }
    else if (value.isBoolean()) {
      value(value.asBoolean());
    }
    else if (value.isString()) {
      value(value.asString());
    }
    else if (value.isJsonArray()) {
      beginArray();
      for (JsonElement e : value.asJsonArray()) {
        write(e);
      }
      endArray();
    }
    else if (value.isJsonObject()) {
      beginObject();
      for (Map.Entry<String, JsonElement> e : value.asJsonObject().entrySet()) {
        name(e.getKey());
        write(e.getValue());
      }
      endObject();

    }
    else {
      throw new UnsupportedOperationException(
        "Couldn't write " + value.getClass()
      );
    }
  }

  /**
   * Begins encoding a new array. Each call to this method must be paired with
   * a call to {@link #endArray}.
   */
  private void beginArray() throws IOException {
    writeDeferredName();
    open(JsonScope.EMPTY_ARRAY, "[");
  }

  /**
   * Ends encoding the current array.
   */
  private void endArray() throws IOException {
    close(JsonScope.EMPTY_ARRAY, JsonScope.NONEMPTY_ARRAY, "]");
  }

  /**
   * Begins encoding a new object. Each call to this method must be paired
   * with a call to {@link #endObject}.
   */
  private void beginObject() throws IOException {
    writeDeferredName();
    open(JsonScope.EMPTY_OBJECT, "{");
  }

  /**
   * Ends encoding the current object.
   */
  private void endObject() throws IOException {
    close(JsonScope.EMPTY_OBJECT, JsonScope.NONEMPTY_OBJECT, "}");
  }

  /**
   * Enters a new scope by appending any necessary whitespace and the given
   * bracket.
   */
  private void open(int empty, String openBracket) throws IOException {
    beforeValue();
    push(empty);
    out.write(openBracket);
  }

  /**
   * Closes the current scope by appending any necessary whitespace and the
   * given bracket.
   */
  private void close(int empty, int nonempty, String closeBracket)
    throws IOException {
    int context = peek();
    if (context != nonempty && context != empty) {
      throw new IllegalStateException("Nesting problem.");
    }
    if (deferredName != null) {
      throw new IllegalStateException("Dangling name: " + deferredName);
    }

    stackSize--;
    if (context == nonempty) {
      newline();
    }
    out.write(closeBracket);
  }

  private void push(int newTop) {
    if (stackSize == stack.length) {
      int[] newStack = new int[stackSize * 2];
      System.arraycopy(stack, 0, newStack, 0, stackSize);
      stack = newStack;
    }
    stack[stackSize++] = newTop;
  }

  /**
   * Returns the value on the top of the stack.
   */
  private int peek() {
    if (stackSize == 0) {
      throw new IllegalStateException("JsonWriter is closed.");
    }
    return stack[stackSize - 1];
  }

  /**
   * Replace the value on the top of the stack with the given value.
   */
  private void replaceTop(int topOfStack) {
    stack[stackSize - 1] = topOfStack;
  }

  /**
   * Encodes the property name.
   * @param name the name of the forthcoming value. May not be null.
   */
  private void name(String name) throws IllegalStateException {
    requireNonNull(name, "name == null");
    if (deferredName != null) {
      throw new IllegalStateException();
    }
    if (stackSize == 0) {
      throw new IllegalStateException("JsonWriter is closed.");
    }
    deferredName = name;
  }

  private void writeDeferredName() throws IOException {
    if (deferredName != null) {
      beforeName();
      string(deferredName);
      deferredName = null;
    }
  }

  /**
   * Encodes {@code value}.
   * @param value the literal string value, or null to encode a null literal.
   */
  private void value(String value) throws IOException {
    if (value == null) {
      return;
    }
    writeDeferredName();
    beforeValue();
    string(value);
  }

  /**
   * Encodes {@code null}.
   */
  private void nullValue() throws IOException {
    if (deferredName != null) {
      if (style.serializeNulls()) {
        writeDeferredName();
      }
      else {
        deferredName = null;
        return; // skip the name and the value
      }
    }
    beforeValue();
    out.write("null");
  }

  private void value(boolean value) throws IOException {
    writeDeferredName();
    beforeValue();
    out.write(value ? "true" : "false");
  }

  /**
   * Encodes {@code value}.
   * @param value a finite value.
   * May not be {@link Double#isNaN() NaNs} or {@link Double#isInfinite() infinities}.
   */
  private void value(BigDecimal value) throws IOException {
    if (value == null) {
      return;
    }

    writeDeferredName();
    String string = value.toPlainString();
    if (string.equals("-Infinity") || string.equals("Infinity") || string.equals("NaN")) {
      throw new IllegalArgumentException("Numeric values must be finite, but was " + value);
    }
    beforeValue();
    out.append(string);
  }

  @Override
  public void flush() throws IOException {
    if (stackSize == 0) {
      throw new IllegalStateException("JsonWriter is closed.");
    }
    out.flush();
  }

  /**
   * Flushes and closes this writer and the underlying {@link Writer}.
   * @throws IOException if the JSON document is incomplete.
   */
  @Override
  public void close() throws IOException {
    out.close();

    int size = stackSize;
    if (size > 1 || size == 1 && stack[size - 1] != JsonScope.NONEMPTY_DOCUMENT) {
      throw new IOException("Incomplete document");
    }
    stackSize = 0;
  }

  private void string(String value) throws IOException {
    String[] replacements = style.htmlSafe() ? HTML_SAFE_REPLACEMENT_CHARS : REPLACEMENT_CHARS;
    out.write("\"");
    int last = 0;
    int length = value.length();
    for (int i = 0; i < length; i++) {
      char c = value.charAt(i);
      String replacement;
      if (c < 128) {
        replacement = replacements[c];
        if (replacement == null) {
          continue;
        }
      }
      else if (c == '\u2028') {
        replacement = "\\u2028";
      }
      else if (c == '\u2029') {
        replacement = "\\u2029";
      }
      else {
        continue;
      }
      if (last < i) {
        out.write(value, last, i - last);
      }
      out.write(replacement);
      last = i + 1;
    }
    if (last < length) {
      out.write(value, last, length - last);
    }
    out.write("\"");
  }

  private void newline() throws IOException {
    String indent = style.indent();
    if (indent == null || indent.isEmpty()) {
      return;
    }

    out.write(style.newLine());
    for (int i = 1, size = stackSize; i < size; i++) {
      out.write(indent);
    }
  }

  /**
   * Inserts any necessary separators and whitespace before a name. Also
   * adjusts the stack to expect the name's value.
   */
  private void beforeName() throws IOException {
    int context = peek();
    if (context == JsonScope.NONEMPTY_OBJECT) { // first in object
      out.write(',');
    }
    else if (context != JsonScope.EMPTY_OBJECT) { // not in an object!
      throw new IllegalStateException("Nesting problem.");
    }
    newline();
    replaceTop(JsonScope.DANGLING_NAME);
  }

  /**
   * Inserts any necessary separators and whitespace before a literal value,
   * inline array, or inline object. Also adjusts the stack to expect either a
   * closing bracket or another element.
   */
  private void beforeValue() throws IOException {
    switch (peek()) {
      case JsonScope.NONEMPTY_DOCUMENT:
      case JsonScope.EMPTY_DOCUMENT: // first in document
        replaceTop(JsonScope.NONEMPTY_DOCUMENT);
        break;

      case JsonScope.EMPTY_ARRAY: // first in array
        replaceTop(JsonScope.NONEMPTY_ARRAY);
        newline();
        break;

      case JsonScope.NONEMPTY_ARRAY: // another in array
        out.append(',');
        newline();
        break;

      case JsonScope.DANGLING_NAME: // value for name
        out.append(style.separator());
        replaceTop(JsonScope.NONEMPTY_OBJECT);
        break;

      default:
        throw new IllegalStateException("Nesting problem.");
    }
  }
}
